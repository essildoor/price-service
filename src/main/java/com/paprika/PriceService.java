package com.paprika;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * Класс предоставляет api для работы с ценами, см. {@link Price}
 * <p>
 * Created by kapitonov on 30.08.2016.
 */
public final class PriceService {

    private final BiFunction<Price, Price, List<Price>> merge = (oldPrice, newPrice) -> {
        List<Price> result;

    };

    private final Comparator<Price> initialPriceComparator = Comparator.comparing(Price::getDepart)
            .thenComparing(Price::getNumber)
            .thenComparing(Price::getNumber)
            .thenComparing(Price::getDepart)
            .thenComparing(Price::getBeginTs)
            .thenComparing(Price::getEndTs);

    /**
     * Обьединяет две коллекции цен
     *
     * @param oldPrices коллекция старых цен
     * @param newPrices коллекция новых цен
     * @return коллекцию объединенных цен
     */
    public Collection<Price> mergePrices(Collection<Price> oldPrices, Collection<Price> newPrices) {
        Objects.requireNonNull(oldPrices);
        Objects.requireNonNull(newPrices);
        if (oldPrices.isEmpty()) {
            return newPrices;
        }
        if (newPrices.isEmpty()) {
            return oldPrices;
        }

        Map<String, List<Price>> oldPriceMap = oldPrices.stream()
                .filter(p -> p != null)
                .sorted(initialPriceComparator)
                .collect(Collectors.groupingBy(Price::getProductCode));

        Map<String, List<Price>> newPriceMap = newPrices.stream()
                .filter(p -> p != null)
                .sorted(initialPriceComparator)
                .collect(Collectors.groupingBy(Price::getProductCode));

        System.out.println();

        return Collections.emptyList();
    }

    public static void main(String[] args) {
        Price old1 = new Price(1L, "111", 1, 1, LocalDateTime.of(2016, 8, 31, 0, 0, 0), LocalDateTime.of(2016, 8, 31, 23, 59, 59), 120000L);
        Price old2 = new Price(2L, "111", 1, 1, LocalDateTime.of(2016, 9, 1, 0, 0, 0), LocalDateTime.of(2016, 9, 1, 23, 59, 59), 125000L);
        Price old3 = new Price(3L, "111", 1, 1, LocalDateTime.of(2016, 8, 30, 0, 0, 0), LocalDateTime.of(2016, 8, 30, 23, 59, 59), 125000L);
        Price old4 = new Price(4L, "333", 1, 1, LocalDateTime.of(2016, 8, 31, 0, 0, 0), LocalDateTime.of(2016, 8, 31, 23, 59, 59), 120000L);
        Price old5 = new Price(5L, "333", 1, 2, LocalDateTime.of(2016, 9, 1, 0, 0, 0), LocalDateTime.of(2016, 9, 1, 23, 59, 59), 125000L);
        Price old6 = new Price(6L, "222", 1, 2, LocalDateTime.of(2016, 8, 30, 0, 0, 0), LocalDateTime.of(2016, 8, 30, 23, 59, 59), 125000L);
        Price old7 = new Price(7L, "222", 2, 2, LocalDateTime.of(2016, 9, 1, 0, 0, 0), LocalDateTime.of(2016, 8, 30, 23, 59, 59), 125000L);
        Price old8 = new Price(8L, "222", 1, 2, LocalDateTime.of(2016, 9, 2, 0, 0, 0), LocalDateTime.of(2016, 8, 30, 23, 59, 59), 125000L);

        Price new1 = new Price(null, "111", 1, 1, LocalDateTime.of(2016, 8, 31, 12, 0, 0), LocalDateTime.of(2016, 8, 31, 20, 59, 59), 110000L);

        new PriceService().mergePrices(Arrays.asList(old1, old2, old3, old4, old5, old6, old7, old8), Collections.singletonList(new1));
    }
}
