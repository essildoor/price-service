package com.paprika;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Класс представление цены. Immutable.
 *
 * Created by kapitonov on 30.08.2016.
 */
public final class Price {

    /**
     * Идентификатор цены в БД
     */
    private Long id;

    /**
     * Код товара
     */
    private final String productCode;

    /**
     * Номер цены
     */
    private final Integer number;

    /**
     * Номер отдела
     */
    private final Integer depart;

    /**
     * Начало действия цены
     */
    private final LocalDateTime beginTs;

    /**
     * Конец действия цены
     */
    private final LocalDateTime endTs;

    /**
     * Значение цены в копейках
     */
    private final Long value;

    public Price(Long id, String productCode, Integer number, Integer depart, LocalDateTime beginTs,
                 LocalDateTime endTs, Long value) {
        Objects.requireNonNull(productCode);
        Objects.requireNonNull(number);
        Objects.requireNonNull(depart);
        Objects.requireNonNull(beginTs);
        Objects.requireNonNull(endTs);
        Objects.requireNonNull(value);

        this.id = id;
        this.productCode = productCode;
        this.number = number;
        this.depart = depart;
        this.beginTs = beginTs;
        this.endTs = endTs;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public String getProductCode() {
        return productCode;
    }

    public Integer getNumber() {
        return number;
    }

    public Integer getDepart() {
        return depart;
    }

    public LocalDateTime getBeginTs() {
        return beginTs;
    }

    public LocalDateTime getEndTs() {
        return endTs;
    }

    public Long getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Price price = (Price) o;

        if (productCode != null ? !productCode.equals(price.productCode) : price.productCode != null) return false;
        if (number != null ? !number.equals(price.number) : price.number != null) return false;
        if (depart != null ? !depart.equals(price.depart) : price.depart != null) return false;
        return value != null ? value.equals(price.value) : price.value == null;

    }

    @Override
    public int hashCode() {
        int result = productCode != null ? productCode.hashCode() : 0;
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (depart != null ? depart.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Price{" +
                "id=" + id +
                ", productCode='" + productCode + '\'' +
                ", number=" + number +
                ", depart=" + depart +
                ", beginTs=" + beginTs +
                ", endTs=" + endTs +
                ", value=" + value +
                '}';
    }
}
